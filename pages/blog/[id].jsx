import Link from 'next/link'
import Image from 'next/image'
import Head from 'next/head'
import Layout from '../../components/Layout'

// data <- es un props que provien del getStaticProps
export default function blogPost({data}) {
    return (
        <Layout>
            <h1>
                {data.id} - {data.title}
            </h1>
            <p>{data.body}</p>
        </Layout>
    )
}

// Esto creara cada ruta de cada Post o Insert: Rutas dinamicas
// Se creara un prop 'params' con el ID almacenado
export async function getStaticPaths() {
    try {
        const res = await fetch('https://jsonplaceholder.typicode.com/posts');
        const data = await res.json();
        const paths = data.map(({id}) => ({params: {id: `${id}`}})); // id es numero, al colocar backstrick se convierte en string
        return {
            paths,
            fallback: false, // GENERA UNA PAGINA 404
        }
    } catch (error) {
        console.log(error)
    }
}

// Se obtendran cada Post, conociendo dinamicamente su ruta por medio de getStaticPaths()
// getStaticPaths() genera un 'params' con el ID de cada post -> prop = paths
// getStaticProps() retorna un prop -> data, el cual puede ser obtenido en la funcion de este componente
export async function getStaticProps ({params}) {
    try {
      const res = await fetch('https://jsonplaceholder.typicode.com/posts/' + params.id);
      const data = await res.json();
      return {
        props: {
          data: data // o -> data 
        }
      }
    } catch (error) {
      console.log(error);
    }
  }
