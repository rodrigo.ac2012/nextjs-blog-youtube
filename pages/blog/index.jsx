import Layout from "../../components/Layout"
import Link from 'next/link'

// funcion Index recibe el prop data
export default function index({data}) {
    return (
        <Layout>
            <h1>Blog</h1>
            {
                //data.map((post) => (
                  //  <div key= {post.id}></div>
                //))
                data.map(({id, title, body}) => (
                    <div key= {id}>
                        <Link href={`/blog/${id}`}>
                          <h3>{id} - {title}</h3>
                        </Link>
                        <p>{body}</p>
                    </div>
                ))
            }
        </Layout>
    )
}

// Se consume API externa y esto se transforma en un Prop
// Los datos se consumen en el momento de la compilacion
export async function getStaticProps () {
    try {
      const res = await fetch('https://jsonplaceholder.typicode.com/posts');
      const data = await res.json();
      return {
        props: {
          data: data // o -> data 
        }
      }
    } catch (error) {
      console.log(error);
    }
  }