import Layout from '../components/Layout'

export default function about() {
    return (
        // Las variables props se deben definir entre la primera etiqueta de Layout
        <Layout
            title="About | next.js"
            description="agregue descripcion"
        >
            <h1>About</h1>
        </Layout>
    )
}
